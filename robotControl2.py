#******************************************************
 # Robotics Interface code
 # 
 # Connects the raspberry pi to a microcontroller via
 # the serial port.  Defines a set of functions for
 # controlling the robot movement, measuring sensors,
 # and configuring parameters
 #
 # Author: Elizabeth Basha
 # Date: Spring 2015
 #*****************************************************
#! /usr/bin/env python

import serial
import string
import struct
import collections
import math

# Command Defines
MOVEXY_ROBOT_COMMAND = 0
MOVEOBS_ROBOT_COMMAND = 1
MOVEBACK_ROBOT_COMMAND = 2
SENSE_ROBOT_COMMAND = 3
CHANGE_SENSE_THRESHOLD_COMMAND = 4
ROTATE_ROBOT_COMMAND = 5

# Robot Defines
WHEEL_RADIUS = 3 # cm
WHEEL_L = 8 # cm, distance between wheels
TICKS_PER_REV = 64  # number of ticks per 1 revolution of the wheel, single channel of encoder only

# Create named tuples for returning values
sensors = collections.namedtuple('Sensors', ['left', 'middle', 'right'])
moved = collections.namedtuple('Movement', ['distance', 'angle', 'time'])
allInfo = collections.namedtuple('AllData', ['distance', 'angle', 'time', 'left', 'middle', 'right'])

# Move robot to X,Y where X is positive
# Returns a named tuple in the order (distance, angle, time)
def moveRobotXY(x, y):
    # Open serial port - GPIOs connected to Serial0
    serialPort=serial.Serial("/dev/serial0",9600)
    #serialPort.open()

    # Write the command to the microcontroller
    serialPort.write(struct.pack('>BBBBB',MOVEXY_ROBOT_COMMAND,(x&0xff),(x>>8)&0xff,(y&0xff),(y>>8)&0xff))

    # Read the response
    msg = serialPort.read(8)
    rightWheelTicks = ord(msg[0])+(ord(msg[1])<<8)
    angleTicks = ord(msg[2])+(ord(msg[3])<<8)
    directionTurned = ord(msg[4])
    moved.time = ord(msg[5])+(ord(msg[6])<<8)

    moved.distance = ((2*math.pi*WHEEL_RADIUS)/TICKS_PER_REV)*rightWheelTicks
    moved.angle = (directionTurned-2)*((2*math.pi*WHEEL_RADIUS)/(TICKS_PER_REV*WHEEL_L))*angleTicks
    
    serialPort.close()

    return moved

# Move robot until obstacle seen
# Returns a named tuple in the order (distance, angle, time, left, middle, right)
def moveRobotObs():
    # Open serial port - GPIOs connected to serial0
    serialPort=serial.Serial("/dev/serial0",9600)
    #serialPort.open()

    # Write the command to the microcontroller
    serialPort.write(struct.pack('>B',MOVEOBS_ROBOT_COMMAND))

    # Read the response
    msg = serialPort.read(13)
    rightWheelTicks = ord(msg[0])+(ord(msg[1])<<8)
    leftWheelTicks = ord(msg[2])+(ord(msg[3])<<8)
    allInfo.time = ord(msg[4])+(ord(msg[5])<<8)
    allInfo.left = ord(msg[6])+(ord(msg[7])<<8)
    allInfo.middle = ord(msg[8])+(ord(msg[9])<<8)
    allInfo.right = ord(msg[10])+(ord(msg[11])<<8)

    angleTicks = rightWheelTicks-leftWheelTicks 
    allInfo.distance = ((2*math.pi*WHEEL_RADIUS)/TICKS_PER_REV)*(rightWheelTicks-abs(angleTicks))
    allInfo.angle = ((2*math.pi*WHEEL_RADIUS)/(TICKS_PER_REV*WHEEL_L))*angleTicks
    
    serialPort.close()

    return allInfo

# Rotate robot by angleToTurn (0 to 2pi)
# Returns a named tuple in the order (distance, angle, time)
def rotateRobot(angleToTurn):
    # Open serial port - GPIOs connected to serial0
    serialPort=serial.Serial("/dev/serial0",9600)
    #serialPort.open()

    angleToTurnInDegrees = int(angleToTurn*(180/math.pi))

    # Write the command to the microcontroller
    serialPort.write(struct.pack('>BBB',ROTATE_ROBOT_COMMAND,(angleToTurnInDegrees&0xff),(angleToTurnInDegrees>>8)&0xff))

    # Read the response
    msg = serialPort.read(6)
    angleTicks = ord(msg[0])+(ord(msg[1])<<8)
    directionTurned = ord(msg[2])
    moved.time = ord(msg[3])+(ord(msg[4])<<8)
   
    moved.distance = 0 
    moved.angle = (directionTurned-2)*((2*math.pi*WHEEL_RADIUS)/(TICKS_PER_REV*WHEEL_L))*angleTicks

    serialPort.close()

    return moved

# Move robot backward by X
# Returns a named tuple in the order (distance, angle, time)
def moveRobotBackX(x):
    # Open serial port - GPIOs connected to serial0
    serialPort=serial.Serial("/dev/serial0",9600)
    #serialPort.open()

    # Write the command to the microcontroller
    serialPort.write(struct.pack('>BBB',MOVEBACK_ROBOT_COMMAND,(x&0xff),(x>>8)&0xff))

    # Read the response
    msg = serialPort.read(5)
    rightWheelTicks = ord(msg[0])+(ord(msg[1])<<8)
    moved.time = ord(msg[2])+(ord(msg[3])<<8)
    moved.angle = 0
    
    moved.distance = ((2*math.pi*WHEEL_RADIUS)/TICKS_PER_REV)*rightWheelTicks

    serialPort.close()

    return moved

# Reads the IR sensors
# Returns a named tuple in the order (left, middle, right)
def getSensors():
    # Open serial port - GPIOs connected to serial0
    serialPort=serial.Serial("/dev/serial0",9600)
    #serialPort.open()

    # Write the command to the microcontroller
    serialPort.write(struct.pack('>B',SENSE_ROBOT_COMMAND))

    # Read the response
    msg = serialPort.read(7)
    sensors.left = ord(msg[0])+(ord(msg[1])<<8)
    sensors.middle = ord(msg[2])+(ord(msg[3])<<8)
    sensors.right = ord(msg[4])+(ord(msg[5])<<8)
    
    serialPort.close()

    return sensors

# Change the sensor threshold
# This will impact when the robot decides it has seen an obstacle
def changeSensorThreshold(newThreshold):
    # Open serial port - GPIOs connected to serial0
    serialPort=serial.Serial("/dev/serial0",9600)
    #serialPort.open()

    # Write the command to the microcontroller
    serialPort.write(struct.pack('>BBB',CHANGE_SENSE_THRESHOLD_COMMAND,newThreshold&0xFF,(newThreshold>>8)&0xff))

    # Read the response
    msg = serialPort.read(2)
    status = ord(msg[0])-48
    
    serialPort.close()

    return status

# Main here
#result = moveRobotXY(0,2)
#print(result.distance, result.angle, result.time)

#result = moveRobotObs()
#print(result.distance, result.angle, result.time)
#print(result.left, result.middle, result.right)

#result = rotateRobot(math.pi/4)
#print(result.distance, result.angle, result.time)

#result = moveRobotBackX(2)
#print(result.distance, result.angle, result.time)

#result = getSensors()
#print(result.left, result.middle, result.right)

#status = changeSensorThreshold(200)
#print(status)
