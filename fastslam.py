#! /usr/bin/env python

import collections
import numpy as np
from numpy.linalg import inv
from numpy.linalg import det
import math
import os
import robotmap as rm
PI = math.pi
forward = collections.namedtuple("Motion",['distance','angle','time'])
turn = collections.namedtuple("Motion",['distance','angle','time'])

def fastslam( rp, mew, covar, movement, dt, Z, S, M, N, errorR, errorQ, maps, mapSize, res ):
    '''
    Input:
        mew - horizontal 2D array(1,N+3) of robot and landmark pose
        covar - 2D array of covariance matrix
        movement - horizontal 1x2 vector of velocity and angular velocity
        Z - a row vector of landmark(s) pose relative to robot [ro, phi]
        S - unique value for each of the landmark
        N - number of landmark
    '''
    numLandmark = len(S)
    v = movement[0]
    w = movement[1]
    #print(numLandmark)
    #print(Z)
    for i in range(M):
        rp[i][:] = np.add(rp[i][:],np.array([[v*math.cos(rp[i][2])*dt,v*math.sin(rp[i][2])*dt,0]]))
    for x in range(numLandmark):
        landmarkPos = np.array([[Z[x,0],Z[x,1],0]])
        nw = []
        if len(mew)==0:
            mewSize = 0
        else:
            mewSize = len(mew[0])
        for k in range(M):
            w = []
            H = []
            psi = []
            newLandmarkPos = []
            for j in range(mewSize):
                deltaX = mew[k][j,0,0]-rp[k,0]
                deltaY = mew[k][j,0,1]-rp[k,1]
                deltaXY = np.array([[deltaX],[deltaY]])
                Q = math.sqrt(np.asscalar(np.dot(deltaXY.T,deltaXY)))
                newLandmarkPos.append(np.array([[Q,math.atan2(deltaY,deltaX)-rp[k,2],0]]))
                H.append((1/Q)*np.array(\
                        [[Q*deltaX,Q*deltaY,0],\
                        [-deltaY,deltaX,0],\
                        [0,0,Q]]))
                psi.append(np.add(np.dot(np.dot(H[j],covar[k][j]),H[j].T),errorQ))
                landmarkDiff = np.subtract(landmarkPos,newLandmarkPos[j])
                w.append((1/(math.sqrt(2*math.pi*det(psi[j]))))*\
                        math.exp(-0.5*(np.dot(np.dot(landmarkDiff,\
                        inv(psi[j])),landmarkDiff.T))))
            w.append(1/M)
            nw.append(max(w))
            c = np.argmax(w)
            newN = max([N,c])
            for j in range(newN+1):
                if(j==N+1 and j==c):
                    if mewSize == 0:
                        mew.append(np.array([[[rp[k,0]+Z[x,0]*math.cos(Z[x,1]+rp[k,2]),\
                                rp[k,1]+Z[x,0]*math.sin(Z[x,1]+rp[k,2]),S[x]]]]))
                        maps[k].setWall(mew[k][j,0,0],mew[k][j,0,1])
                    else:
                        tempMew = mew[k]
                        mew[k] = np.concatenate((tempMew,np.array([[[rp[k,0]+Z[x,0]*math.cos(Z[x,1]+rp[k,2]),\
                                rp[k,1]+Z[x,0]*math.sin(Z[x,1]+rp[k,2]),S[x]]]])),axis=0)
                        maps[k].setWall(mew[k][j,0,0],mew[k][j,0,1])
                    deltaX = mew[k][j,0,0]-rp[k,0]
                    deltaY = mew[k][j,0,1]-rp[k,1]
                    deltaXY = np.array([[deltaX],[deltaY]])
                    Q = math.sqrt(np.asscalar(np.dot(deltaXY.T,deltaXY)))
                    Hc = (1/Q)*np.array(\
                        [[Q*deltaX,Q*deltaY,0],\
                        [-deltaY,deltaX,0],\
                        [0,0,Q]])
                    if mewSize == 0:
                        covar.append(np.array([np.dot(inv(Hc),np.dot(errorQ,inv(Hc).T))]))
                    else:
                        tempCovar = covar[k]
                        covar[k] = np.concatenate((tempCovar,np.array([np.dot(inv(Hc),np.dot(errorQ,inv(Hc).T))])),axis=0)
                else:
                    if(j<=N and j==c):
                        K = np.dot(np.dot(covar[k][j],H[j].T),inv(psi[j]))
                        landmarkDiff = np.subtract(landmarkPos,newLandmarkPos[j])
                        mew[k][j] = np.add(mew[k][j],np.dot(K,landmarkDiff.T).T)
                        covar[k][j] = np.dot(np.subtract(np.identity(3),np.dot(K,H[j])),covar[k][j])
        wIndex = np.random.choice(M,M,True,nw/np.sum(nw))
        nrp = np.zeros([M,3])
        nmew = []
        ncovar = []
        nmaps = []
        for i in range(M):
            nrp[i,:] = np.copy(rp[wIndex[i],:])
            nmew.append(np.copy(mew[wIndex[i]]))
            ncovar.append(np.copy(covar[wIndex[i]]))
            tempMap = rm.robotMap(mapSize,res)
            tempMap.copyMap(maps[wIndex[i]])
            nmaps.append(tempMap)
        rp = nrp
        mew = nmew
        covar = ncovar
        maps = nmaps
        N = newN
    '''
    print('new mew')
    print(mew)
    print('rp')
    print(rp)
    '''
    return [rp,mew,covar,newN,maps]

def fastExplore( rp, M, N, errorQ, maps, moveSpd, turnSpd ):
    H = 99999
    controls = []
    dist = 5
    #for k in range(M):
    rpIndex = np.random.choice(M)
    paths = [np.squeeze(np.add(rp[rpIndex],np.array([[dist,0,0]]))),\
             np.squeeze(np.add(rp[rpIndex],np.array([[-dist,0,0]]))),\
             np.squeeze(np.add(rp[rpIndex],np.array([[0,dist,0]]))),\
             np.squeeze(np.add(rp[rpIndex],np.array([[0,-dist,0]]))),\
             np.squeeze(np.add(rp[rpIndex],np.array([[-dist,dist,0]]))),\
             np.squeeze(np.add(rp[rpIndex],np.array([[dist,-dist,0]]))),\
             np.squeeze(np.add(rp[rpIndex],np.array([[dist,dist,0]]))),\
             np.squeeze(np.add(rp[rpIndex],np.array([[-dist,-dist,0]])))]
    ctrls = []
    counter = 0
    for path in paths:
        forward.angle = 0
        turn.distance = 0
        cp = np.squeeze(rp[rpIndex])
        gp = path
        [forward.distance, turn.angle] = goalDistanceAngle(cp,gp)
        forward.time = forward.distance/moveSpd
        turn.time = turn.angle/turnSpd
        ctrls.append([turn,forward])
        nrp = np.zeros([M,3])
        for k in range(M):
            nrp[k] = np.copy(rp[k])
            rAngle = turn.angle
            if rAngle > PI:
                rAngle = rAngle - 2*PI
            nrp[k,2] = rAngle
        nrp = pseudoFastMeasure( nrp, [moveSpd,0], forward.time, M)
        xMean = np.sum(nrp,axis=0)[0]/M
        yMean = np.sum(nrp,axis=0)[1]/M
        varX = 0
        varY = 0
        covarXY = 0
        for k in range(M):
            tempX = nrp[k][0]-xMean
            tempY = nrp[k][1]-yMean
            varX = varX + pow(tempX,2)
            varY = varY + pow(tempY,2)
            covarXY = covarXY + tempX*tempY
        varX = varX/M
        varY = varY/M
        covarXY = covarXY/M
        covarMat = np.array([[varX,covarXY],[covarXY,varY]])
        tempVal = det(covarMat)
        if tempVal <= 0:
            h = 0
        else:
            h = 0.5*np.log(tempVal)
        for k in range(M):
            h = h + maps[k].getVal(nrp[k][0],nrp[k][1])/M
        if(h < H):
            H = h
            seqIndex = counter
        counter = counter + 1
    return ctrls[seqIndex]

def fastMeasure( rp, movement, dt, M):

    v = movement[0]
    w = movement[1]
    for k in range(M):
        rp[k][:] = np.add(rp[k][:],np.array([[v*math.cos(rp[k][2])*dt,v*math.sin(rp[k][2])*dt,0]]))

def pseudoFastMeasure( rp, movement, dt, M):

    v = movement[0]
    w = movement[1]
    nrp = np.zeros([M,3])
    for k in range(M):
        nrp[k][:] = np.add(rp[k][:],np.array([[v*math.cos(rp[k][2])*dt,v*math.sin(rp[k][2])*dt,0]]))
    return nrp

def goalDistanceAngle( cp, gp ):
    opp = gp[0] - cp[0]
    adj = gp[1] - cp[1]
    dist = math.sqrt(pow(opp,2)+pow(adj,2))

    if( opp == 0 and  gp[1] >= cp[1] ):
        theta = PI/2
    elif( opp ==0 and gp[1] <= cp[1] ):
        theta = 1.5*PI
    elif( adj == 0 and gp[0] >= cp[0] ):
        theta = 0
    elif( adj == 0 and gp[0] <= cp[0] ):
        theta = PI

    if( opp > 0 and adj > 0 ):
        theta = PI/2 - math.atan(opp/adj)
    if( opp < 0 and adj > 0 ):
        theta = PI/2 + abs(math.atan(opp/adj))
    if( opp < 0 and adj < 0 ):
        theta = 1.5*PI - math.atan(opp/adj)
    if( opp > 0 and adj < 0 ):
        theta = 1.5*PI - math.atan(opp/adj)

    return [ dist, theta ]

def turn(rAngle,gAngle):
    if rAngle < 0:
        rAngle = rAngle + 2*PI
    if gAngle < PI:
        angleTurn = rAngle - gAngle
        oppRAngle = gAngle + PI
        if rAngle <= oppRAngle and rAngle >= gAngle:
            pass
        else:
            angleTurn = angleTurn - 2*PI
            if angleTurn <= -2*PI:
                angleTurn = angleTurn + 2*PI
    else:
        angleTurn = gAngle - rAngle
        oppRAngle = gAngle - PI
        if rAngle >= oppRAngle and rAngle <= gAngle:
            angleTurn = -1*angleTurn
        else:
            angleTurn = angleTurn - 2*PI
            if angleTurn <= -2*PI:
                angleTurn = angleTurn + 2*PI
            angleTurn = -1*angleTurn
    return -angleTurn
