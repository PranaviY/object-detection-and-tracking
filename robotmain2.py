#! /usr/bin/env python

import math
import numpy as np
import robotControl
import collections
import time
import os
import fastslam
import robotmap as rm
import random
import robotColor
import signal
import sys
from threading import Thread

sensors = collections.namedtuple('Sensors',['left','middle','right'])
NUMPARTICLES = 3
MOVESPEED = 13.2
TURNSPEED = 1.47
MOVEMENT = [MOVESPEED,0]
WAITTIME = 1.5

def signalHandler( signum, frame ):
    print("Exiting program")
    TERMINATE = 1
    sys.exit()

signal.signal( signal.SIGINT, signalHandler )
controlObj = np.array([-1,0])
maps = []
rp = np.zeros([NUMPARTICLES,3])
TERMINATE = 0

def main():
    global maps
    global rp
    #initialization
    mew = []
    covar = []
    errorR = np.array([[.2,0,0],[0,.2,0],[0,0,.04]])
    errorQ = np.array([[1,0,0],[0,1,0],[0,0,1]])
    initX = 0
    initY = 0
    initTheta = 0
    N = -1
    dispConst = 1
    wallThreshold = 15
    #initialize map
    mapSize = 250
    res = 2
    wallNum = 100
    trackingObj = Thread(target=robotColor.object_det,args=(controlObj,))
    threadPrinter = Thread(target=printSystem,args=())
    threadPrinter.setDaemon(True)
    trackingObj.setDaemon(True)
    trackingObj.start()
    # Initializing particles
    for i in range(NUMPARTICLES):
        if random.random() > .5:
            xOffsetSign = -1
        else:
            xOffsetSign = 1
        if random.random() > .5:
            yOffsetSign = -1
        else:
            yOffsetSign = 1
        if random.random() > .5:
            tOffsetSign = -1
        else:
            tOffsetSign = 1
        rp[i,:] = [initX+xOffsetSign*dispConst*random.random(),initY+yOffsetSign*dispConst*random.random(),initTheta + tOffsetSign*0.17*random.random()]
        maps.append(rm.robotMap(mapSize,res))
    threadPrinter.start()
    # If sensors is not working don't do anything
    while(1):
        getSensors()
        if(sensors.left < 0 or sensors.right < 0 or sensors.middle < 0):
            continue
        else:
            break
    # wait for opencv to get ready
    time.sleep(5)
    # move forward until see wall then do fast slam
    while(1):
        while(controlObj[1]):
            if(controlObj[0] == 0):
                #turn left
                turn(rp,0.1745)
                systemWait(.5)
            elif(controlObj[0] == 1):
                #turn Right
                turn(rp,-0.1745)
                systemWait(.5)
            elif(controlObj[0] == 2):
                #forward
                moveForward(rp,5)
                systemWait(.5)
            elif(controlObj[0] == 3):
                #stop/wait
                time.sleep(1)
        # If CONTROL is False then explore
        time.sleep(3)
        while(controlObj[1] == 0):
            updateSensors()
            if(sensors.left < wallThreshold or sensors.right < wallThreshold):
                Z = []
                S = []
                if(sensors.left < wallThreshold):
                    Z = np.array([[sensors.left, .78539]])
                    S.append(wallNum)
                    wallNum = wallNum + 1
                if(sensors.right < wallThreshold):
                    if len(Z) == 0:
                        Z = np.array([[sensors.right, -.78539]])
                    else:
                        Z = np.concatenate((Z,np.array([[sensors.right, -.78539]])))
                    S.append(wallNum)
                    wallNum = wallNum + 1
                # Do fast slam here
                [rp,mew,covar,N,maps] = fastslam.fastslam(rp,mew,covar,[0,0],0,Z,S,NUMPARTICLES,N,errorR,errorQ,maps,mapSize,res)

                ctrls=fastslam.fastExplore(rp,NUMPARTICLES,N,errorQ,maps,MOVESPEED,TURNSPEED)
                execute(rp,ctrls)
                time.sleep(1)
                continue
            if(controlObj[1] == 0):
                moveForward(rp,10)
            else:
                break

def turnLeft(angle):
    #NOTE:The robot still has bug where the angle is always divided by 2
    result = robotControl.rotateRobot(math.pi)
    angle = angle + math.pi/2
    if angle > math.pi:
        angle = angle - 2*math.pi
    #print(angle)
    return angle

def turnRight(angle):
    #NOTE:The robot still has bug where the angle is always divided by 2
    result = robotControl.rotateRobot(-math.pi)
    angle = angle - math.pi/2
    if angle < -math.pi:
        angle = angle + 2*math.pi
    #print(angle)
    return angle

def turn(rp,angle):
    result = robotControl.rotateRobot(angle*2)
    for k in range(NUMPARTICLES):
        tempAngle = rp[k][2] + result.angle
        if(tempAngle > math.pi):
            tempAngle = tempAngle - 2*math.pi
        if(tempAngle < -math.pi):
            tempAngle = tempAngle + 2*math.pi
        rp[k][2] = tempAngle

def execute( rp, controlSequence ):
    for control in controlSequence:
        #print("Moving: "+str(control.distance))
        #print("Turning: "+str(control.angle))
        #print("Time: "+str(control.time))
        if(control.distance == 0):
            turn(rp, control.angle)
            #print('turning: ' + str(control.angle))
        if(control.angle == 0):
            moveForward(rp,control.distance)
            #print('forwarding: ' + str(control.distance))

def moveForward( rp, dist ):
        result = robotControl.moveRobotXY(int(dist),0)
        disp = result.distance
        if disp:
            dt = disp/MOVESPEED
            move = [disp/dt, dt]
        else:
            dt = 0
            move = [ 0, dt ]
        fastslam.fastMeasure(rp, move, dt, NUMPARTICLES)

def updateSensors():
    startTime = time.time()
    while( time.time() - startTime <= WAITTIME  and controlObj[1]==0):
        getSensors()

def systemWait( val ):
    startTime = time.time()
    while( time.time() - startTime <= val ):
        continue

def getSensors():
    global sensors
    result = robotControl.getSensors()
    sensors.left = (1024-result.left)*(26)/(1024) - 5
    sensors.right = (1024-result.right)*(26)/(1024) - 5
    sensors.middle = (1024-result.middle)*(26)/(1024) - 5
    #print([sensors.left,sensors.middle,sensors.right])

def printSystem():
    while(~TERMINATE):
        os.system('clear')
        particleNum = 0
        print("Robot Pose")
        print("X-Coordinate: " + str(rp[particleNum][0]))
        print("Y-Coordinate: " + str(rp[particleNum][1]))
        print("Sensor Values")
        print("Left: "  + str(sensors.left) + " Middle: " + str(sensors.middle) + \
                "Right: " + str(sensors.right))
        if( controlObj[1] ):
            print("Robot Mode: Tracking")
            if(controlObj[0] == 0):
                #turn left
                print("Turning Left")
                #turn(rp,0.349)
            elif(controlObj[0] == 1):
                print("Turning Right")
                #turn Right
                #turn(rp,-0.349)
            elif(controlObj[0] == 2):
                print("Moving Forward")
                #forward
                #moveForward(rp,5)
            elif(controlObj[0] == 3):
                print("Stop")
        else:
            print("Robot Mode: Dora The Exploring")
        time.sleep(1)

if __name__ == '__main__':
    main()
