from picamera.array import PiRGBArray
from picamera import PiCamera
import time
import cv2
import numpy as np
#import matplotlib.pyplot as plt

#Detect input object using goodFeaturesToTrack
"""image_input = cv2.imread('resized_input.jpg')
#r_image_input = cv2.resize(image_input,(640,640))
hsv_input=cv2.cvtColor(image_input,cv2.COLOR_BGR2HSV)

lower_yellow=np.array([20,30,30])
upper_yellow=np.array([30,255,255])
mask_input=cv2.inRange(hsv_input,lower_yellow,upper_yellow)
detect_input=cv2.bitwise_and(image_input,image_input,mask=mask_input)
#cv2.imwrite('detect_input.jpg',detect_input)
gray=cv2.cvtColor(detect_input,cv2.COLOR_BGR2GRAY)
gray=np.float32(gray)
dst = cv2.goodFeaturesToTrack(gray,90,0.01,10)
dst = np.int0(dst)

for corner in dst:
    x,y =corner.ravel()
    cv2.circle(image_input,(x,y),3,255,-1)
cv2.imshow('corners',image_input)"""

#Find COnnected components and centroid of largest area component
#Track the centroid and give commands to the robot to follow the
#object
def object_det(obj):
    camera =PiCamera()
    camera.resolution=(640,480)
    camera.framerate=60
    rawCapture= PiRGBArray(camera,size=(640,480))
    time.sleep(0.1)
    #sift = cv2.xfeatures2d.SIFT_create()
    numFrames = 120
    stateArray = [0]

    for frame in camera.capture_continuous(rawCapture,format="bgr",use_video_port=True):
        obj[1]=0;
        control_action=''
        image=frame.array
        #img2 = cv2.imread(img,0)
        hsv=cv2.cvtColor(image,cv2.COLOR_BGR2HSV)
        lower_yellow=np.array([15,100,100]);
        upper_yellow=np.array([30,255,255]);
        mask=cv2.inRange(hsv,lower_yellow,upper_yellow)
        mask=cv2.erode(mask,None,iterations=2)
        mask=cv2.dilate(mask,None,iterations=2)
        #cv2.imshow('mask',mask)
        #res=cv2.bitwise_and(image,image,mask=mask)-- to put the yellow color back 

        #cv2.imwrite('res.jpg',res)
        #good features
        #cv2.imshow('yellow_in_frame',res)
        #gray=cv2.cvtColor(res,cv2.COLOR_BGR2GRAY)
        #gray_1=np.float32(gray)
        #dst = cv2.goodFeaturesToTrack(gray_1,100,0.01,10)
        #threshold = 0.1* dst.max()

        #dst = np.int0(dst)

        #for corner in dst:
            #x,y =corner.ravel()
            #cv2.circle(image,(x,y),3,255,-1)

        #cv2.imshow('Robot sees',image)
        #cv2.imshow('gray image',gray)

        """ret,th1=cv2.threshold(gray,75,255,cv2.THRESH_BINARY)
        contours = cv2.findContours(th1.copy(),cv2.RETR_EXTERNAL,\
                cv2.CHAIN_APPROX_SIMPLE)

        contours_in = np.array(contours).astype(np.int32)
        cv2.drawContours(th1,[contours_in],-1,(0,255,0),3)"""
        #cv2.imshow('thresh image',th1)
        connectivity = 8
        components_output= cv2.connectedComponentsWithStats(mask,connectivity,cv2.CV_8U)
        num_labels=components_output[0]
        stats =components_output[2]
        centroid=components_output[3]
        area=[]
        if num_labels >0:
            for i in range(1,num_labels):
                val=stats[i,cv2.CC_STAT_AREA]
                area.append(val)
        if len(area)==0:
            #print("object not found-Explore")
            #obj[1]=0
            stateArray.append(0)
        else:
            #print(max(area),area.index(max(area)))
            stateArray.append(0)
            maxarea = max(area)
            #print(area)
            if maxarea >= 600 and maxarea<12800:
                #print(area)
                stateArray.append(1)
                #print("found my object")
                #boolean = True
                #obj[1]=1
                maxarea_comp=area.index(max(area))+1
                centroid_max=centroid[maxarea_comp]
                #print(centroid_max)
                
                centroid_max = np.int0(centroid_max)
                x,y =centroid_max.ravel()
                #print(x y)
                
                #Pass control action to the robot
                if x >= 0 and x<= 213:
                    #print("centre in left most")
                    #control_action= 'left'
                    obj[0]=0
                #elif x>128 and x<= 256:
                    #print("centre towards left")
                    #control_action= 'left'
                    #obj[0]=0
                elif x>213 and x<=426:
                    #print("in the centre")
                    #control_action= 'forward'
                    obj[0]=2
                elif x>426  and x<=640:
                    #print("towards right")
                    #control_action='right'
                    obj[0]=1
                #elif x>512 and x<=640:
                   # print("right most")
                    #control_action= 'right'
                    #obj[0]=1
                cv2.circle(mask,(x,y),5,(0,255,0),-1)
            elif maxarea >= 12800:
                #boolean = True
                stateArray.append(1)
                #obj[1]=1
                #print('object is too close')
                #control_action= 'stop'
                obj[0]=3
            cv2.imshow('mask',mask)
        #print(stateArray)
        if (any(stateArray)):
            obj[1]=1
        else:
            obj[1]=0

        if len(stateArray) >60:
            stateArray.pop(0)
        #print(obj)
        #ret_list.append(str(control_action))
        #ret_list.append(boolean)
        #return ret_list
        #detect using SIFT
        """kp1,des1=sift.detectAndCompute(detect_input,None)
        kp2,des2=sift.detectAndCompute(res,None)
        bf = cv2.BFMatcher()
        matches=bf.knnMatch(des1,des1,k=2)

        good=[]
        for m,n in matches:
            if m.distance<0.1*n.distance:
                good.append([m])
        img3 =cv2.drawMatchesKnn(detect_input,kp1,res,kp2,good,None,flags=2)
        #cv2.imwrite('img3.jpg',img3)
        #break"""
        key=cv2.waitKey(33)
        rawCapture.truncate(0)
        if key== 33:
            print("pressed a")
            QuitProgram()
    

"""def main():
    obj =np.array([0,0])
    object_det(obj)
    print("out of function \n")
    print(obj[0])"""

